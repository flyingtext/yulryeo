
# 율려(Yulryeo)
소수(素數) 저장소 율려(律呂)입니다. 

Prime number data repository Yulryeo.

## 소수 추출 방법(Prime number extraction method)
GPSC(Generalized Prime Sequence Conjecture)를 응용하여 자체개발한 [GPSCSearch](https://github.com/flyingtext/GPSCSearch)를 포팅한 GPSCSearch.Net(추후 공개 예정)으로 추출하였습니다. GPSC에 관해서는 제가 작성한 다음 문헌 참조 부탁드립니다.

I applied the Generalized Prime Sequence Conjecture (GPSC) to extract prime numbers with GPSCSearch.Net (coming soon), which is a port of my program [GPSCSearch](https://github.com/flyingtext/GPSCSearch). For GPSC, please refer to the following papers written by me.

* [TechRxiv](https://doi.org/10.36227/techrxiv.22306411)
* [Zenodo](https://doi.org/10.5281/zenodo.7754381)

## 소수 추출 정확도(Prime number extraction accuracy)
본 저장소에 업로드 된 소수들은 밀러 라빈 알고리즘(반복 최소 3회)을 2회 검증한 뒤 판별된 숫자입니다. 다만, 알고리즘 상 모든 소수가 계산에 선별된 것은 아닙니다. 즉, 업로드된 소수의 판별 결과는 사실이나, 빠른 연산 속도를 위해 모든 숫자에 대해 소수 선별을 진행하지는 않았습니다. 율려 저장소 초기 버전에는 소수가 아닌 수를 포함하였으나 저장 공간 문제로 배제하였습니다.

The primes uploaded to this repository have been verified twice using the Miller-Rabin primality test (at least 3 iterations). However, not all primes are selected for calculation by the algorithm, i.e., the uploaded prime numbers are true prime numbers, but it is not performed on all numbers for faster computation. The initial version of the Yulryeo repository included non-prime numbers, but they were excluded due to storage space issues.

## 각종 기록(Various records)

### 2023/03/23

* 소수 286256개 (2023/03/28에서 오류 교정)
* 발견된 최대 소수 값 76자리
```
1633508400379514260465059612388540789704359918393109357539804830657312315819
```
* 소수 아닌 수 1949503개 (2023/03/28에서 배제)
* 발견된 최대 소수 아닌 수 값 76자리 8개 (값은 파일 참조)

* 286256 prime numbers
* Largest prime value found 76 digits
```
1633508400379514260465059612388540789704359918393109357539804830657312315819
```
* 1949503 non-prime numbers 
* Maximum non-prime number of values found 76 digits (8 numbers) (see file for values)

### 2023/03/28

* 기존 소수 결과 검산 후 처음부터 다시 연산
* 소수 5944309개
* 발견된 소수 값 최대 878자리 
```
81535760541415137873350261003853448667139604732821715925814911774398545253336974002888766631800273604506670213346656683850034068186236167248954298267160785998759559536022247658551141840855715765189010817877814549005509976407235328990326100973883009988211496108239654907208773841675133092016850503518081066354974074601289604263448290780792151529601569135317522728157056704708076603495912755407200290572066040716814473232966330950683919932087911335940992956786073122255071085599968310702000574927336634217479667576921627694861578436705525588583200809628809517257006924909791899494622923980642353762521543390554221065740526013708888159675745618284940257772453406970318531179476973267923652520411984069785379642352345325303128444818260913971304951498061886837566940172709539064057718803263574357305965917839410598108196145521797051525851976043241965293809790362371243120718268453641
```


## 업데이트 로그(Update log)

* 2023/03/23 - 업로드 시작
* 2023/03/28 - 마당 2

* 2023/03/23 - Started uploading
* 2023/03/28 - Version 2

## 링크 및 소개(Links and introduction)

한의사입니다. 프로그래밍, 수학에 관심이 많습니다.

I am a Korean medicine doctor. Interested in programming, math.

* Blog [flyingtext](https://flyingtext.blog)
* E-mail [flyingtext@nate.com](flyingtext@nate.com)
* ORCID [0000-0001-9610-0994](https://orcid.org/0000-0001-9610-0994)
* [TechRxiv](https://www.techrxiv.org/authors/Jihyeon_Yoon/13214070), [SSRN](https://papers.ssrn.com/sol3/cf_dev/AbsByAuth.cfm?per_id=5334925)