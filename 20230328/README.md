# 2023/03/28 Upload

## 파일 설명(File explanation)

### prime_numbers.json

원본 계산 결과 데이터입니다. 검산 결과 약 180~190여 항목에서 오류를 포함하고 있었습니다. 이에 다시 필터를 실행하였습니다.

The data from the original calculation. The calculation showed that about 180~190 items contained errors, so the data were filtered again.

### filtered_prime_numbers.json

원본 계산 결과 데이터에서 필터를 실행한 후 소수가 아닌 수들을 배제한 데이터입니다. JSON 포맷입니다.

Data that excludes non-prime numbers after running a filter on the original calculation result data. JSON format.

### filtered_prime_numbers.csv

원본 계산 결과 데이터에서 필터를 실행한 후 소수가 아닌 수들을 배제한 데이터입니다. CSV 포맷입니다.

Data that excludes non-prime numbers after running a filter on the original calculation result data. CSV format.